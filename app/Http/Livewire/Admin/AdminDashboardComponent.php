<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class AdminDashboardComponent extends Component
{
    
    public function render()
    {
        $data['users'] = DB::table('users')
                ->orderBy('id', 'desc')
                ->paginate(config('app.row')); 
              
        return view('livewire.admin.admin-dashboard-component',$data)->layout('layouts.base');

    }
    // public function create_user()
    // {
    //     // return view('livewire.admin.create_user-component')->layout('layouts.base');
    //     return view('livewire.user.user-dashboard-component')->layout('layouts.base');
    
    // }

    // public function index() 
    // { $data['users'] = DB::table('users')
    //     ->orderBy('id', 'desc')
    //     ->paginate(config('app.row')); 
    //     return view('users.index', $data);
    // }
}
