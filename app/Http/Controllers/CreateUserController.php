<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
class CreateUserController extends Controller
{
    public function create_user()
    {
        return view('livewire.admin.create_user-component')->layout('layouts.base');
    
    }
}
